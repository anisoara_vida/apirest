<?php
	// Connexion à la base
	include("connexion.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	function readTickets()
	{
		global $conn;
		$query = "SELECT * FROM ticket";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	function readTicket($id=0)
	{
		global $conn;
		$query = "SELECT * FROM ticket";
		if($id != 0)
		{
			$query .= " WHERE id=".$id." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	function createTicket()
	{
		global $conn;
		$id = $_POST["id"];
    $date = date("Y-m-d");
		$description = $_POST["description"];
		$niveauSeverite = $_POST["niveauSeverite"];
		echo $query="INSERT INTO ticket(id, date, description, niveauSeverite) VALUES('".$id."', '".$date."', '".$description."', '".$niveauSeverite."')";
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Ticket ajoute avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'ERREUR!.'. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	function updateTicket($id)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
        $date = date("Y-m-d");
        $description = $_PUT["description"];
        $niveauSeverite = $_PUT["niveauSeverite"];
		$query="UPDATE ticket SET date='".$date."', description='".$description."', niveauSeverite='".$niveauSeverite."' WHERE id=".$id;

		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Ticket mis a jour avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour du ticket. '. mysqli_error($conn)
			);

		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	function deleteTicket($id)
	{
		global $conn;
		$query = "DELETE FROM ticket WHERE id=".$id;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Ticket supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du ticket a echouee. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	switch($request_method)
	{

		case 'GET':
			if(!empty($_GET["id"]))
			{
				$id=intval($_GET["id"]);
				readTicket($id);
			}
			else
			{
				readTickets();
			}
			break;
        default:
        header("HTTP/1.0 405 Method Not Allowed");
        break;

		case 'POST':
			// Ajouter un ticket
			createTicket();
			break;

		case 'PUT':
			// Modifier un ticket
			$id = intval($_GET["id"]);
			updateTicket($id);
			break;

        case 'DELETE':
        // Supprimer un ticket
        $id = intval($_GET["id"]);
        deleteTicket($id);
        break;


	}
?>
